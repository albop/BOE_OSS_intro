import numpy
import pandas
from ipywidgets import *

parameters = {
    'T': 45,
    'sigma': 1,
    'n_slides': 30,
    'max_duration': 60,
    'tps': 120,
    'vol_tps': 60
}

def simulate(parameters):
    n_slides = parameters['n_slides']
    tps = parameters['tps']/60
    std = parameters['vol_tps']/60

    slides = numpy.arange(n_slides)

    time_per_slide = tps + numpy.random.randn(n_slides)*std
    questions = ((5+numpy.random.random(n_slides)*2)*(numpy.random.random(n_slides)>0.9))
    time_per_slide_with_questions = time_per_slide + questions

    cumulated_time = time_per_slide.cumsum()
    cumulated_time_with_questions = time_per_slide_with_questions.cumsum()

    cols = numpy.concatenate([cumulated_time[:,None], cumulated_time_with_questions[:,None]], axis=1)
    df = pandas.DataFrame(cols,columns=["No questions","Questions"],index=slides)
    return df

import numpy as np
from IPython.display import display
from bqplot import (
    OrdinalScale, LinearScale, Bars, Lines, Axis, Figure
)

duration = parameters['max_duration']
n_slides = parameters['n_slides']
# np.random.seed(0)

# sim = draw_simulation(parameters)
sim = simulate(parameters)
x_data = sim.index
y1_init = sim["No questions"]
y2_init = sim["Questions"]

x_ord = OrdinalScale(domain=[i for i in range(n_slides)])
y_sc = LinearScale(min=0, max=65)


line1 = Lines(x=x_data, y=y1_init, scales={'x': x_ord, 'y': y_sc},
             stroke_width=3, colors=['blue'], display_legend=True, labels=['Without questions'])

line2 = Lines(x=x_data, y=y2_init, scales={'x': x_ord, 'y': y_sc},
             stroke_width=3, colors=['red'], display_legend=True, labels=['With questions'])
line2.visible = False

line3 = Lines(x=x_data, y=y2_init*0+duration, scales={'x': x_ord, 'y': y_sc},
             stroke_width=3, colors=['black'], display_legend=True, labels=['Max Duration'])

def hide(el):
    el.visible = not el.visible
line1.on_legend_click(lambda u,v: hide(line1))
line2.on_legend_click(lambda u,v: hide(line2))

# lines =
#     [Lines(x=x_data, y=y_init, scales={'x': x_ord, 'y': y_sc},
#              stroke_width=3, colors=['red'], display_legend=True, labels=['Line chart'])

ax_x = Axis(scale=x_ord, grid_lines='solid', label='Slides')
ax_y = Axis(scale=y_sc, orientation='vertical', tick_format='0.2f',
            grid_lines='solid', label='Time')

figure = Figure(marks=[line1, line2, line3], axes=[ax_x, ax_y], title='Will I finish on time ?',
       legend_location='bottom-right')


# In[5]:

def rerun(sender):
    import time
    N=10
#     line.colors=['red']*N
#     line.opacities = [1]*N
    df = simulate(parameters)
    for t in range(df.shape[0]):
        x = df.index[t]
        y2 = numpy.array(df["Questions"][t])
        y1 = numpy.array(df["No questions"][t])
        if t == 0:
            line1.x = [x]
            line1.y = [y1]
            line2.x = [x]
            line2.y = [y2]
        else:
            line1.x = numpy.concatenate([line1.x, [x]])
            line2.x = numpy.concatenate([line2.x, [x]])
            line1.y = numpy.concatenate([line1.y, [y1]])
            line2.y = numpy.concatenate([line2.y, [y2]])
            time.sleep(0.1)
button_rerun = Button(description="Rerun")
button_rerun.on_click(rerun)


# In[8]:

is1 = IntSlider(description="Duration", value=60, min=30,max=90,step=5, orientation='horizontal')
def update_is1(*args):
    val = is1.value
    line3.y = line3.y*0+val
is1.observe(update_is1)

is2 = IntSlider(description="Number of slides", value=parameters['n_slides']) #, orientation='vertical')
is3 = IntSlider(description="Time per slide", value=parameters['tps'], min=60, max=240, step=15) #, orientation='vertical')
is4 = IntSlider(description="Volatility per slide", value=parameters['vol_tps']) #, orientation='vertical')

def update_is2(*args):
    v = is2.value
    x_ord.domain = [*range(v)]
    parameters['n_slides'] = v
is2.observe(update_is2)

def update_is3(*args):
    v = is3.value
    parameters['tps'] = v
is3.observe(update_is3)

def update_is4(*args):
    v = is4.value
    parameters['vol_tps'] = v
is4.observe(update_is4)



b1 = HBox( [ Box([figure, HBox([ button_rerun, is1] )])] )
b2 = Box([
        is2,
        is3,
        is4
])
accordion = widgets.Accordion(children=[b1, b2])
accordion.set_title(0, 'Simulation')
accordion.set_title(1, 'Parameters')
accordion
